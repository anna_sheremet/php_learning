@extends('welcome')
<div class="container">
    <div class="row mt-5justify-content-center align-items-center">
        <div class="col-md-8">
            <h1>{{$post->title}}</h1>
            <p>{{$post->body}}</p>
            <p><small>{{\App\Models\User::find($post->user_id)->name}}</small></p>
        </div>
        <div class="col-md-4">
            @forelse($comments as $comment)
                <div class="card text-end mt-5" style="width: 18rem;">
                    <div class="card-body">
                        <h5 id="comment_{{$comment->id}}" class="card-title">{{\App\Models\User::find($comment->user_id)->name}}</h5>
                        <p class="card-text">{{$comment->comment}}</p>
                        <a class="btn btn-primary" onclick="reply_comment({{$comment->id}})">Reply</a>
                    </div>
                </div>
            @empty
                <h3 class="p-3">No comments yet</h3>
            @endforelse
        </div>
    </div>
    <hr/>
    <div class="row justify-content-center align-items-center">
        <div class="col-md-5 mt-5">
            <h1>Adding comment to this post</h1>
            <form action="{{route('add-comment')}}" method="post" autocomplete="on">
                @csrf
                <input required type="hidden" name="post_id"  value="{{$post->id}}">
                <div class="mb-3">
                    <label for="textTextarea1" class="form-label">Text for comment</label>
                    <textarea required class="form-control" id="textTextarea1" name="comment" rows="3" placeholder="Please, type your text here"></textarea>
                </div>
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary btn-lg" name="submit" value="Send comment">
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center align-items-center mb-5">
        <div class="col-md-6 align-items-center text-center mt-5">
            <a href="{{route('home')}}" class="btn btn-lg btn-outline-primary">Back to homepage</a>
        </div>
    </div>
</div>
