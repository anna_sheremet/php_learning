@extends('welcome')
@section('content')
    <div class="content">
        @forelse($posts as $post)
            <div class="row mb-2">
                <div class="col-md-6">
                    <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <h3 class="mb-0 text-success">{{$post->title}}</h3>
                            <div class="mb-1 text-muted">Author: {{ \App\Models\User::find($post->user_id)->name}}</div>
                            <p class="mb-auto">{{$post->body}}</p>
                            <a href="{{Route('post')}}" class="stretched-link">Continue reading</a>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-12 align-self-center text-center">
                <h2>No posts yet.</h2>
            </div>
        @endforelse
            <div class="row justify-content-center align-items-center mt-4">
                <div class="col-md-12 text-center">
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>

            <div class="row justify-content-center align-items-center">
                <div class="col-md-6 align-items-center text-center mt-5">
                    <a href="{{route('add-post')}}" class="btn btn-lg btn-outline-primary">Add post</a>
                </div>
            </div>
    </div>
@endsection


