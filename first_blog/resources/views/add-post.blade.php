@extends('welcome')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 mt-5">
            <h1>Adding post to your web page</h1>
            <form action="{{route('add-post')}}" method="post" autocomplete="on">
                @csrf
                <div class="mb-3">
                    <label for="textInput1" class="form-label">Enter title</label>
                    <input required type="text" class="form-control" name="title" id="textInput1" placeholder="Please, type a title for your post here...">
                </div>
                <div class="mb-3">
                    <label for="textTextarea1" class="form-label">Enter text</label>
                    <textarea required class="form-control" id="textTextarea1" name="text" rows="3" placeholder="Please, type a text for your post here..."></textarea>
                </div>
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary btn-lg" name="submit" value="Send post">
                </div>
            </form>
        </div>
    </div>
</div>
