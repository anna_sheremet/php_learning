<?php

use App\Http\Controllers\AddPostController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('home', function () {
    return redirect(\route('home'));
});
Route::get('/post/{id?}', [PostController::class, 'index'])->name('post');

Route::middleware('auth')->group(function(){
    Route::get('/add-post', [AddPostController::class, 'index'])->name('add-post');

    Route::post('/add-post', [AddPostController::class, 'addPost'])->name('add-post');

    Route::post('/add-comment', [PostController::class, 'add_comment'])->name('add-comment');
});
