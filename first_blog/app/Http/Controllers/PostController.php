<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index($id = 1) {
        $post = Post::find($id);
        $comments = Comment::all()->where('post_id', '=', $post->id);
        return view('post', ['post' => $post, 'comments' => $comments]);
    }

    public function add_comment(Request $request) {
        $comment = new Comment();
        $comment->message = $request->input('message');
        $comment->post_id = $request->input('post_id');
        $comment->user_id = Auth::id();
        $comment->save();

        return redirect()->back();
    }
}
