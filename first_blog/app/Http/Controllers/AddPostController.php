<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AddPostController extends Controller
{
    public function index() {
            return view('add-post');
    }

    public function addPost(Request $request) {

        $post = new Post();
        $post->title = $request->input('title');
        $post->body = $request->input('text');
        $post->user_id = Auth::id();
        $post->save();

        return redirect(route('home'));
    }
}
