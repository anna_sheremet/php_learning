<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class FirstException extends Exception{
    public function __construct($message = " ", $code = 0)
    {
        parent::__construct($message, $code);
        $this->go();
    }
    private function go(){
        echo "You get exception 1" . "<br>";
    }
}

class Play{
    public function run(){
        $a = rand(1,2);
        if($a == 1){
            $e1 = new FirstException();
        }else{
            echo "All right" . "<br>";
        }
    }
}

$game1 = new Play();
$game1->run();
$game2 = new Play();
$game2->run();
$game3 = new Play();
$game3->run();
$game4 = new Play();
$game4->run();
$game5 = new Play();
$game5->run();
