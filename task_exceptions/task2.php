<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function dividing($b){
    if(!$b){
        throw new Exception('Division by zero') ;
    }
    return 1/$b;
}

try{
    echo dividing(0);
}catch (Exception $e){
    echo $e->getMessage();
}
