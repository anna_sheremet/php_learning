<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index($id){
        if($id % 2 == 0){
            return view('string');
        } else {
            return "hello, your number is odd";
        }
    }
}
