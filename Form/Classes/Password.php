<?php
class WrongPasswordException extends Exception{
    public function __construct($message = " ", $code = 0)
    {
        parent::__construct($message, $code);
        $this->message();
    }
    private function message(){
        echo "Wrong password was entered" . "<br>";
    }
}