<?php
namespace Form;
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require_once 'Classes/Enter.php';
use Form\Classes\Authentication;
If(isset($_POST['submit'])){
    $user = new Authentication();
    $user->check($_POST['login'], $_POST['password']);
}

?>

<!doctype html>
<html lang="en">
<head>
   <meta charset="UTF-8">
    <title>Authentication</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <h1>Enter your data</h1>
<form method="post" class="my_form">
    <label for="login">Enter your login</label>
    <input name="login" type="text" id="login">
    <label for="password">Enter your password</label>
    <input name="password" type="password" id="password">
    <input type="submit" value="Send" name="submit" id="submit" class="button">
</form>
</body>
</html>