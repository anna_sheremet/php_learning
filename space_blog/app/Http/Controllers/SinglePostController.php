<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class SinglePostController extends Controller
{
    public function index($id = 1){
        $post = Post::find($id);
        return view('post', ['post' => $post]);
    }
}
