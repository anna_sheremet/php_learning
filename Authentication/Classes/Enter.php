<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'Connection.php';

class Authentication
{
    public $checker = false;
    public $passwordChecker = false;
    public function check($login, $password)
    {
        $extract = new Connection;
        $arrayOfLogins = $extract->getBy("login");
        foreach($arrayOfLogins as $person)
        {
            if($person["login"] == $login)
            {
                $this->checker = true;
                echo $login . "<br>";
                break;
            }
        }
        if(!$this->checker)
        {
            throw new WrongLoginException;
        }


        if($this->checker)
        {
            $arrayOfPassword = $extract->getPassword("password", $login);
            foreach($arrayOfPassword as $element)
            {
                if($element["password"] == $password)
                {
                    $this->passwordChecker = true;
                    echo $password;
                    break;
                }
            }
            if(!$this->passwordChecker)
            {
                throw new WrongPasswordException;
            }
        }


        if(!$this->checker && !$this->passwordChecker){
            throw new AuthFailedException;
        }
    }
}

$user = new Authentication();
$user->check("user1", 456);