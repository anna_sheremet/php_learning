<?php

class Connection{
    private $pdo;

    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=127.0.0.1; dbname=task8', 'root', '');
    }

    public function getBy($columnName)
    {
        $sth = $this->pdo->prepare("SELECT {$columnName}  FROM users ");
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPassword($columnName, $login)
    {
        $sth = $this->pdo->prepare("SELECT {$columnName} FROM users WHERE login = \"{$login}\"");
        $sth->execute();
        return $sth->fetchAll();
    }
}

